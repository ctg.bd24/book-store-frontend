import './App.scss';
import PublicPage from './components/public-page/PublicPage';

function App() {
  return (
    <>
      <PublicPage />
    </>
  );
}

export default App;
